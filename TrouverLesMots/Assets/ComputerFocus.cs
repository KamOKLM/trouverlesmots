﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ComputerFocus : MonoBehaviour
{
    public static ComputerFocus INSTANCE = null;
    public GameObject ComputerCanvas;
    public TextMeshProUGUI DialogText;
    public GameObject TypingText;
    public string StartKey;
    public SoundManager MusicManager;
    public GameObject LapisLazuli;
    public AudioSource SonPC;

    [Space]
    [Header("Answer 1")]
    public Button FirstButton;
    public TextMeshProUGUI FirstButtonText;
    public string FirstAnswer;
    [Space]
    [Header("Answer 1")]
    public Button SecondButton;
    public TextMeshProUGUI SecondButtonText;
    public string SecondAnswer;

    private bool isNext;
    private Animator anim;
    private bool isStart = true;

    private void Awake()
    {
        if(INSTANCE == null)
        {
            INSTANCE = this;
        } else
        {
            Destroy(gameObject);
        }
        LapisLazuli.SetActive(false);
        anim = GetComponent<Animator>();
        ComputerCanvas.SetActive(true);
        FirstButton.gameObject.SetActive(false);
        SecondButton.gameObject.SetActive(false);
        TypingText.SetActive(false);
        isStart = true;
    }

    private IEnumerator Start()
    {
        yield return new WaitForSeconds(1f);
        StartCoroutine(PutDialog(JsonManager.INSTANCE.GetContent(StartKey), false));
    }

    public void OpenComputer()
    {
        ComputerCanvas.SetActive(true);
    }


    public void CloseComputer()
    {
        if (TypingText.activeSelf)
            return;
        ComputerCanvas.SetActive(false);
        if (isStart || isNext)
        {
            isStart = false;
            MusicManager.ToEnquete();
        }
        if (isNext)
        {
            isNext = false;
            GameManager.INSTANCE.NextNarrativeStep();
        }
    }

    public void AddAnswer(string computerAnswer, string dialog)
    {
        if (!FirstButton.gameObject.activeSelf)
        {
            FirstButtonText.text = computerAnswer;
            FirstButton.gameObject.SetActive(true);
            FirstAnswer = dialog;
        } else //SecondButton
        {
            SecondButtonText.text = computerAnswer;
            SecondButton.gameObject.SetActive(true);
            SecondAnswer = dialog;
        }
    }

    public void LaunchFirstAnswer()
    {
        StartCoroutine(PutDialog(FirstAnswer, FirstButtonText.text.Contains("Roppi")));
        ClearAnswer();
    }

    public void LaunchSecondAnswer()
    {
        StartCoroutine(PutDialog(SecondAnswer, false));
        ClearAnswer();
    }

    private IEnumerator PutDialog(string dialog, bool lapislazuli)
    {
        string[] msg = dialog.Split('\n');
        TypingText.SetActive(true);
        yield return new WaitForSeconds(2f);
        foreach(string line in msg)
        {
            DialogText.text += "\n\r" + line;
            yield return new WaitForSecondsRealtime(Mathf.Max(1.5f,0.3f * line.Split(' ').Length));
        }
        TypingText.SetActive(false);
        if (lapislazuli)
        {
            StartCoroutine(LaunchPC3());
        }
    }

    private IEnumerator LaunchPC3()
    {
        yield return new WaitForSeconds(0.8f);
        LapisLazuli.SetActive(true);//TODO: faire l'objet qui flash gentillement et lancer le reste du dialogue (PC3 du coup)
        yield return new WaitForSeconds(3.8f);
        LapisLazuli.SetActive(false);
        StartCoroutine(PutDialog(JsonManager.INSTANCE.GetContent("PC3"), false));
    }

    private void ClearAnswer()
    {
        isNext = true;
        MusicManager.ToDark();
        FirstButton.gameObject.SetActive(false);
        SecondButton.gameObject.SetActive(false);
    }


    private void OnMouseEnter()
    {
        anim.SetBool("hover", true);
    }

    private void OnMouseExit()
    {
        anim.SetBool("hover", false);

    }


    private void OnMouseDown()
    {
        if (!GameManager.SCENE_CLIKABLE)
            return;
        ComputerCanvas.SetActive(true);
        SonPC.Play();
        GameManager.SCENE_CLIKABLE = false;
    }
}
