﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class JsonManager : MonoBehaviour
{
    public static JsonManager INSTANCE = null;
    public static string Localization = "";
    private Dictionary<string, string> gameText;

    private void Awake()
    {
        if (INSTANCE == null)
        {
            INSTANCE = this;
        }
        else
        {
            Destroy(gameObject);
        }
        LoadText();
    }


    // Start is called before the first frame update
    private void LoadText()
    {
        gameText = new Dictionary<string, string>();
        JsonList text = ParserJson("Story" + Localization + ".json");
        FeedDict(text);
    }

    public JsonList ParserJson(string path)
    {
        string fullPath = Path.Combine(Application.streamingAssetsPath, path);
        string content = File.ReadAllText(fullPath);
        return JsonUtility.FromJson<JsonList>(content);
    }

    public void FeedDict(JsonList items)
    {
        foreach (var item in items.text)
        {
            gameText.Add(item.Key, item.Content);
        }
    }

    public string GetContent(string key)
    {
        if (gameText.ContainsKey(key))
        {
            return gameText[key];
        }
        else
        {
            return "ERROR: key " + key + " not found";
        }
    }

}
