﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;

public class ObjectDescription : MonoBehaviour
{
    public static ObjectDescription INSTANCE = null;
    public GameObject DescriptionCanvas;
    public TextMeshProUGUI TextField;

    private string[] msgBuffer;
    private int currentBuffer;
    private bool thinking = false;
    private UnityEvent onFinishedEvent;

    private void Awake()
    {
        if(INSTANCE == null)
        {
            INSTANCE = this;
        } else
        {
            Destroy(gameObject);
        }
        DescriptionCanvas.SetActive(false);
    }


    public void WriteDescription(string msg, UnityEvent events)
    {
        msgBuffer = msg.Split('\n');
        onFinishedEvent = events;
        currentBuffer = 0;
        DescriptionCanvas.SetActive(true);
        StartCoroutine(TypeText(msgBuffer[currentBuffer]));
    }

    private IEnumerator TypeText(string msg)
    {
        TextField.text = "";
        yield return new WaitForEndOfFrame();
        if (!String.IsNullOrEmpty(msg))
        {
            thinking = true;
            bool richText = false;
            string buffer = "";
            foreach (char c in msg)
            {
                if (!thinking)
                {
                    TextField.text = msg;
                    break;
                }
                if(c == '<')
                {
                    richText = true;
                    buffer = "";
                }
                if (!richText)
                {
                    TextField.text += c;
                    yield return new WaitForSeconds(0.02f);
                } else
                {
                    buffer += c;
                }
                if (c == '>')
                {
                    richText = false;
                    TextField.text += buffer;
                }
                
            }
            thinking = false;
        }
    }
    public void ReceiveClick()
    {
        if (thinking)
        {
            thinking = false;
        } else if (currentBuffer < msgBuffer.Length - 1)
        {
            currentBuffer++;
            StartCoroutine(TypeText(msgBuffer[currentBuffer]));
        } else
        {
            if(onFinishedEvent != null)
            {
                onFinishedEvent.Invoke();
                onFinishedEvent = null;
            }
            DescriptionCanvas.SetActive(false);
            GameManager.SCENE_CLIKABLE = true;
        }
    }
}
