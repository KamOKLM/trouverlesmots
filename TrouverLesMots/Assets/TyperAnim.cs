﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

[RequireComponent(typeof(TextMeshProUGUI))]
public class TyperAnim : MonoBehaviour
{

    private TextMeshProUGUI typing;
    private float delay = 0.5f;
    private float currentTime = 0f;
    // Start is called before the first frame update

    private void OnEnable()
    {
        if(typing == null)
        {
            typing = GetComponent<TextMeshProUGUI>();
        }
        typing.text = ".";
        
    }

    private void Update()
    {
        currentTime += Time.deltaTime;
        if(currentTime > delay)
        {
            currentTime = 0f;
            if (typing.text.Length >= 5)
            {
                typing.text = ".";
            }
            else
            {
                typing.text += ".";
            }
        }
    }
}
