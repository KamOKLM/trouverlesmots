﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomAppear : MonoBehaviour
{
    public float Delay = 2f;
    private List<SpriteRenderer> renderers;

    private void Awake()
    {
        renderers = new List<SpriteRenderer>();
        FindRenderer(transform);
        foreach(SpriteRenderer sp in renderers)
        {
            sp.color = new Color(0, 0, 0, 1);
        }
    }

    private void Start()
    {
        foreach (SpriteRenderer sp in renderers)
        {
            sp.color = new Color(0, 0, 0, 1);
        }
    }

    private void FindRenderer(Transform parent)
    {
        foreach(Transform child in parent)
        {
            SpriteRenderer sp = child.GetComponent<SpriteRenderer>();
            if(sp != null)
            {
                renderers.Add(sp);
            }
            FindRenderer(child);
        }
    }

    private void Update()
    {
            foreach (SpriteRenderer sp in renderers)
            {
                sp.color = new Color(sp.color.r + Time.deltaTime / Delay,
                    sp.color.g + Time.deltaTime / Delay,
                    sp.color.b + Time.deltaTime / Delay,
                    1);
            }
    }


}
