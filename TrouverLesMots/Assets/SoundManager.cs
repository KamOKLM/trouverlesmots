﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    public AudioSource EnqueteAudio;
    public AudioSource DarkAudio;
    public float Delay = 1f;
    private bool toDark = false;
    private bool toEnquete = false;
    private bool toStop = false;

    public void Awake()
    {
        DarkAudio.volume = 1f;
        EnqueteAudio.volume = 0f;
    }

    public void ToDark()
    {
        toDark = true;
        toEnquete = false;
        toStop = false;
    }

    public void ToEnquete()
    {
        toEnquete = true;
        toDark = false;
        toStop = false;
    }

    public void ToStop()
    {
        toEnquete = false;
        toDark = false;
        toStop = true;
    }



    private void Update()
    {
        if (toDark)
        {
            DarkAudio.volume = Mathf.Min(1f, DarkAudio.volume + Time.deltaTime / Delay);
            EnqueteAudio.volume = Mathf.Max(0f, EnqueteAudio.volume - Time.deltaTime / Delay);
        }
        else if (toEnquete)
        {
            EnqueteAudio.volume = Mathf.Min(1f, EnqueteAudio.volume + Time.deltaTime / Delay);
            DarkAudio.volume = Mathf.Max(0f, DarkAudio.volume - Time.deltaTime / Delay);
        } else if (toStop)
        {
            EnqueteAudio.volume = Mathf.Max(0f, EnqueteAudio.volume - Time.deltaTime / Delay);
            DarkAudio.volume = Mathf.Max(0f, DarkAudio.volume - Time.deltaTime / Delay);
        }
    }
}
