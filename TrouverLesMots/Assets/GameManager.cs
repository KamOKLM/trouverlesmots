﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;

public class GameManager : MonoBehaviour
{
    public static GameManager INSTANCE = null;
    public static bool SCENE_CLIKABLE = false;
    public NarrativeEvent[] Narration;
    public int NarrativeLvl = -1;

    // Use this for initialization
    private void Awake()
    {
        if(INSTANCE == null)
        {
            INSTANCE = this;
        } else
        {
            Destroy(gameObject);
        }
        SCENE_CLIKABLE = false;
    }


    public void UnlockScene()
    {
        SCENE_CLIKABLE = true;
    }

    public void NextNarrativeStep()
    {
        NarrativeLvl++;
        Narration[NarrativeLvl].Events.Invoke();
    }
}

[System.Serializable]
public class NarrativeEvent
{
    public string Name;
    public UnityEvent Events;
}
