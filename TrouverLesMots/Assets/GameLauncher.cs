﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameLauncher : MonoBehaviour
{
    private bool loading = false;

    private void Start()
    {
        loading = false;
    }
    public void LaunchFR()
    {
        if (loading)
            return;
        loading = true;
        JsonManager.Localization = "FR";
        SceneManager.LoadSceneAsync(1);
    }

    public void LaunchEN()
    {
        if (loading)
            return;
        loading = true;
        JsonManager.Localization = "EN";
        SceneManager.LoadSceneAsync(1);
    }
}
