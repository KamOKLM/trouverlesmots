﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class InteractableObject : MonoBehaviour
{

    [Space]
    [Header("Description")]
    public string TextKey;
    public UnityEvent OnDescriptionFinished;

    [Space]
    [Header("Computer")]
    public string ComputerAnswerKey;
    public string DialogKey;
    public bool IsComputerAnswer = false;

    private Animator anim;

    private void Awake()
    {
        anim = GetComponent<Animator>();
    }

    private void OnMouseEnter()
    {
        anim.SetBool("hover", true);
    }

    private void OnMouseExit()
    {
        anim.SetBool("hover", false);
    }

    private void OnMouseDown()
    {
        if (!GameManager.SCENE_CLIKABLE)
            return;
        GameManager.SCENE_CLIKABLE = false;
        if (IsComputerAnswer && !string.IsNullOrEmpty(ComputerAnswerKey))
        {
            ComputerFocus.INSTANCE.AddAnswer(JsonManager.INSTANCE.GetContent(ComputerAnswerKey), JsonManager.INSTANCE.GetContent(DialogKey));
        }
        string msg = JsonManager.INSTANCE.GetContent(TextKey);
        ObjectDescription.INSTANCE.WriteDescription(msg, OnDescriptionFinished);
    }
}
